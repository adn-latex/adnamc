
% Adin Ramirez adin (at) ic.unicamp.br
\def\fileversion{1.0}
\def\filedate{2014/05/02}

\typeout{Package: `adnamc (adn automultiplechoice)' \fileversion\space <\filedate>}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{adnamc}[\filedate\space\fileversion]

\RequirePackage{pgfkeys}
\RequirePackage{pgfopts}
\RequirePackage{etoolbox}
% Package options
\def\article@classoptions{letter}% list all options that should be passed to article here
\def\amc@packageoptions{lang=EN}% list all default options passed to amc

\newif\ifadnamc@circle%
\newif\ifadnamc@box%
\pgfkeys{%
  /adnamc/.cd,
  circle/.is if={adnamc@circle},
  circle/.default=true,
  % Options for the ID (we need the names without space due to LaTeX variable pass)
  id length/.store in=\@id@length,
  idlength/.store in=\@id@length,
  id name/.store in=\@id@name,
  idname/.store in=\@id@name,
  %
  box/.is if={adnamc@box},
  box/.default=true,
  %
  article/.code={\edef\article@classoptions{\article@classoptions,#1}},
  % unknown keys are assumed to be options to be passed to the amc package
  .unknown/.code={\edef\amc@packageoptions{\amc@packageoptions,\pgfkeyscurrentname=#1}},
}

% defaults
\pgfkeys{%
  /adnamc/.cd,
  % put defaults here
  circle,
  box,
  %
  id length=8,
  id name=id,
}

%% Language strings
% patch the definition of NAC in the spanish toolbox
\def\nac{\AMC@loc@none}

\def\adn@AMC@loc@EN{%
  \def\AMC@loc@question{Question}
  \def\AMC@loc@explain{\textit{\textbf{Explanation: }}}
  \def\AMC@loc@instructions{Instructions}
  \def\AMC@loc@nameinstructions{Mark your ID, and write your name and last names below.}
  \def\AMC@loc@namesurname{Name(s) and last name(s):}
}
\def\adn@AMC@loc@ES{%
  \def\AMC@loc@question{Pregunta}
  \def\AMC@loc@none{Ninguna de estas respuestas es correcta.}
  \def\AMC@loc@explain{\textit{\textbf{Explicaci\'on: }}}
  \def\AMC@loc@instructions{Instrucciones}
  \def\AMC@loc@nameinstructions{Marque su ID, y escriba sus nombres y apellidos abajo.}
  \def\AMC@loc@namesurname{Nombre(s) y apellido(s):}
}
\def\adn@AMC@loc@PT{%
  \def\AMC@loc@question{Pergunta}
  \def\AMC@loc@none{Nenhuma dessas respostas está correta.}
  \def\AMC@loc@explain{\textit{\textbf{Explica\c{c}\~ao: }}}
  \def\AMC@loc@instructions{Instru\c{c}\~oes}
  \def\AMC@loc@nameinstructions{Marque seu ID, e escreva seu nome e sobrenome embaixo.}
  \def\AMC@loc@namesurname{Nome(s) e sobrenome(s):}
}

% process options key-value
\ProcessPgfOptions{/adnamc}
% process options code
\ifadnamc@circle%
  \gdef\adnamc@opt{\AMCboxDimensions{shape=oval,width=2.5ex,height=2.5ex}}%
\else%
  \gdef\adnamc@opt{}%
\fi%

\LoadClass[\article@classoptions]{article}
\RequirePackage[\amc@packageoptions]{automultiplechoice}
\RequirePackage{tikz}
% load my options
\AtBeginDocument{\adnamc@opt}

% Handle box setup manually since there is no turn off option on the package
\ifadnamc@box%
  \AMC@qbloctrue%
\else%
  \AMC@qblocfalse%
\fi%


% for patching
\RequirePackage{etoolbox}
\RequirePackage{regexpatch}

% for logos
\RequirePackage{adn}

% for using square and blacksquare in instructions
\RequirePackage{amssymb}

%% Define languages
\edef\@lang{\AMC@lang@code}
\ifstrequal{\@lang}{}{\gdef\@lang{EN}}{}

\ifcsdef{adn@AMC@loc@\@lang}{%
  \expandafter\csname adn@AMC@loc@\@lang\endcsname
}{}

% My variables for holding information
\def\@instructions{}
\newcommand{\instructions}[1]{\def\@instructions{#1}}

\def\@rightheader{}
\def\@leftheader{}
\newcommand{\rightheader}[1]{\def\@rightheader{#1}}
\newcommand{\leftheader}[1]{\def\@leftheader{#1}}

% depracated (message)
\newcommand{\coursecode}[1]{\PackageWarning{adnamc}{\noexpand\coursecode is deprecated, prefer \noexpand\leftheader or \noexpand\rightheader commands}\def\@rightheader{#1}}
\newcommand{\testname}[1]{\PackageWarning{adnamc}{\\testname is deprecated, prefer \\leftheader or \\rightheader commands}\def\@leftheader{#1}}



% patch the question beginning (question sheet)
\def\AMCbeginQuestion#1#2{\vspace{\AMCformVSpace}\par\noindent{\bf #1.} #2}
% patch the question beginning (form answer)
\def\AMCformQuestion#1{\vspace{\AMCformVSpace}\par{\bf \AMC@loc@question \makebox[1.5em][l]{#1:}}}
% patch the space before every question
\def\AMCbeforeQuestion{\vspace{\AMCformVSpace}\par}
% patch indent on the elements
\patchcmd{\element}{{#2}}{{\par#2}}{}{}

% patch space
\AMCinterBquest=1.5ex



\newcommand{\@make@header}{%
  \ifboolexpr{ test {\ifdefempty{\leftlogo}} or test {\ifdefempty{\rightlogo}} }{%
    % there is no double logo
    \fancyhead[C]{\begin{minipage}{0.3\textwidth}\getlogo\end{minipage}\hspace{0.2\textwidth} \begin{minipage}{0.3\textwidth}\AMCIDBoxesABC\end{minipage}}%
  }{%
    % set a double logo
    \fancyhead[C]{{\hfill\begin{minipage}{0.32\textwidth}\centering\leftlogo\end{minipage}\hfill \begin{minipage}{0.32\textwidth}\centering\AMCIDBoxesABC\end{minipage}\hfill\begin{minipage}{0.32\textwidth}\centering\rightlogo\end{minipage}\hfill}}%
  }%
}%

% creates the header for the automultichoice
\newcommand{\makename}{%
  \@make@header%
  % código del curso
  \def\@test@{\@rightheader\@leftheader}%
  \def\@myhead{%
  \ifdefempty{\@test@}{}{% at least one is not empty
    \noindent{\bf \@leftheader \hfill \@rightheader}%
  }%
  \vspace{10pt}\\%<- this enter is needed
  {% scoped
    \let\groupopen\groupopendemo%
    \let\groupclose\groupclosedemo%
    \ifdefempty{\@instructions}{}{\noindent\textbf{\AMC@loc@instructions.} \@instructions}
  }%
  \vspace{10pt}\\%<- this enter is needed
  % Identification
  % Sets the new aspect for the students code
  \let\oldAMChspace\AMCcodeHspace%
  \let\oldAMCvspace\AMCcodeVspace%
  \AMCcodeHspace=.2em%
  \AMCcodeVspace=.2em%
  {\setlength{\parindent}{0pt}\hspace*{\fill}\AMCcode{\@id@name}{\@id@length}\hspace*{\fill}%
  % Restate the old values
  \AMCcodeHspace=\oldAMChspace%
  \AMCcodeVspace=\oldAMCvspace%
  \begin{minipage}[b]{6.5cm}%
  $\longleftarrow{}$ \AMC@loc@nameinstructions%
  \vspace{3ex}%
  \hfill\namefield{\fbox{ % leave one space
      \begin{minipage}{.9\linewidth}
        \AMC@loc@namesurname\\\\
        \vspace*{.5cm}\dotfill\\
        \vspace*{.5cm}\dotfill
        \vspace*{1mm}
      \end{minipage}
    }}\hfill\vspace{1ex}\end{minipage}\hspace*{\fill}%
  }%
  \vspace{10pt}}%
  % setup depending number of columns
  \if@twocolumn%
    \twocolumn[\@myhead]%
  \else%
    \@myhead%
  \fi%
}

% Header with instructions alone
\newcommand{\makeinstructions}{%
  \@make@header
  % código del curso
  \def\@test@{\@rightheader\@leftheader}
  \def\@myhead{%
  \ifdefempty{\@test@}{}{% at least one is not empty
    \noindent{\bf \@leftheader \hfill \@rightheader}%
  }
  \vspace{10pt}\\%<- this enter is needed
  \ifdefempty{\@instructions}{}{\noindent\textbf{\AMC@loc@instructions.} \@instructions}
  \vspace{10pt}\\}%<- this enter is needed
  % setup depending number of columns
  \if@twocolumn%
    \twocolumn[\@myhead]%
  \else%
    \@myhead%
  \fi%
}

% patch the onecopy to reset automatically figures and tables
\let\oldonecopy\onecopy
\renewcommand{\onecopy}[2]{\oldonecopy{#1}{%
  % reset
  \setcounter{figure}{0}%
  \setcounter{table}{0}%
  % content
  #2%
}}

% patch for two column answers
% patch multicol answers
\RequirePackage{multicol}
\def\twocolanswers{%
  \BeforeBeginEnvironment{choices}{\setlength{\multicolsep}{3pt}\begin{multicols}{2}\AMCBoxedAnswers}%
  \AfterEndEnvironment{choices}{\end{multicols}}%
}

% patch ~ in AMC, so when using spanish it doesn't break
\patchcmd{\AMCopen@lines}{~ }%<- this space is needed
{\hspace*{0pt}}
{}{}

% macro to draw marks in instructions
\newcommand{\@AMC@answer@box}[4]{%
\ifcsname AMC@answerBox@\endcsname% manage new version
    \AMC@answerBox@{#1}{#2}{#3}{#4}%
  \else% manage old version
    \gdef\AMC@checkedbox{1}
    \AMC@boxedchar{#1}{#4}{#3}{#2}%
  \fi%
}
\newcommand{\emptybox}{\raisebox{-.2ex}{\@AMC@answer@box{}{}{}{}}}
\newcommand{\fillbox}{\raisebox{-.2ex}{\@AMC@answer@box{}{\AMC@checkedbox}{}{}}}

% macro for changing form on solutions
\let\ifsolutions\ifAMC@correchead

% patch AMCOpen to hide boxes on solutions
\let\oldAMCopenShow\AMCopenShow
\renewcommand{\AMCopenShow}[2]{%
\ifsolutions%
  \oldAMCopenShow{#1, lines=0,lineheight=0pt,dots=false,framerulecol=white}{#2}%
\else%
  \oldAMCopenShow{#1}{#2}%
\fi%
}

% Add functionality to hold boxes
% http://tex.stackexchange.com/q/195005/7561
% Mi código para generar cajas para código automaticamente
\newcounter{myboxcounter}
\setcounter{myboxcounter}{0}
\newenvironment{specialbox}[1][]{%
  \ifstrempty{#1}{%
    \addtocounter{myboxcounter}{1}%
    \expandafter\newsavebox\csname foobox\roman{myboxcounter}\endcsname%
    \global\expandafter\setbox\csname foobox\roman{myboxcounter}\endcsname\hbox\bgroup\color@setgroup\ignorespaces%
  }{%
    \expandafter\newsavebox\csname foobox#1\endcsname%
    \global\expandafter\setbox\csname foobox#1\endcsname\hbox\bgroup\color@setgroup\ignorespaces%
  }
}{%
  \color@endgroup\egroup
}

\newcommand{\insertbox}[1][]{%\stepcounter{myboxcounter}%
  \ifstrempty{#1}%
  {\expandafter\usebox\csname foobox\roman{myboxcounter}\endcsname}%
  {\expandafter\usebox\csname foobox#1\endcsname}%
}

% Add functionality for group of questions
\RequirePackage{dashrule}
\newcommand{\groupopen}{$\triangleright$~\hdashrule[0.4ex]{0.5\linewidth}{1.pt}{5pt}\newline}
\newcommand{\groupclose}{\strut\hfill\hdashrule[0.4ex]{0.5\linewidth}{1.pt}{5pt}~$\triangleleft$}
\newcommand{\groupopendemo}[1][20pt]{$\triangleright$\hdashrule[0.4ex]{#1}{1pt}{2.5pt}}
\newcommand{\groupclosedemo}[1][20pt]{\hdashrule[0.4ex]{#1}{1pt}{2.5pt}$\triangleleft$}
\newcommand{\groupelement}[2]{%
\element{#1}{%
\vspace{\AMCformVSpace}\noindent\groupopen\hspace{.8ex}
#2 %<- the enter is needed (i.e., the space) in order to force a new line
\groupclose}}

% Table question
\newdimen\@AMCtableL
\newdimen\@AMCtableTextL
\newdimen\@AMCtableC
\newdimen\@AMCtableCh
\newcount\@AMCtableI
\newcount\@AMCtableColNum
\newcount\@AMColdCount
% whether each answer should have more than one correct answer
\newbool{@AMCusemultianswer}% \newif\if@AMCusemultianswer equivalent
% whether we should use the number of options given as answers to convert the question to multiple answer one
\newbool{@AMCuseoptionsnumbers}% \newif\if@AMCuseoptionsnumber equivalent
\newbool{@multiques}
\newbool{@multioptions}

\def\headerLeft{}
\def\headerChoices{}
\newcommand\@makeheader[2]{\par\noindent\begin{minipage}{\@AMCtableL}\colorbox{white}{#1#2}\end{minipage}\par}
\def\makeheader{\@makeheader{\headerLeft}{\headerChoices}}

\pgfkeys{%
  table width/.default=\linewidth,
  table width/.code={\@AMCtableL=#1},
  left text width/.code={\@AMCtableTextL=#1},
  left text width/.default=-1000pt,
  set text width/.code={\ifdim\@AMCtableTextL=-1000pt \@AMCtableTextL=\dimexpr\@AMCtableL-\@AMCtableColNum\@AMCtableC\relax\fi},
  num columns/.default=1,
  num columns/.code={\@AMCtableColNum #1\relax},
  column width/.default=1cm,
  column width/.code={\@AMCtableC=#1},
  column height/.default=1.1em,
  column height/.code={\@AMCtableCh=#1},
  % header of table
  table header/.code={\global\def\headerChoices{#1}},
  table header/.default=,
  table header left text/.code={\global\def\headerLeft{#1}},
  table header left text/.default={\AMCtableLeft{\hfil}},
  table header left text,
  score/.default=1,
  score/.store in=\@AMCrowChoiceScore,
  % whether to force the whole table to use multiple answers for all the options
  use multi answer/.default=true,
  use multi answer/.is if=@AMCusemultianswer,
  % whether to use the values of the options to decide if it is multiple answer or single
  use options number/.default=true,
  use options number/.is if=@AMCuseoptionsnumbers,
}

\newif\if@added
% #1 pgfkeys options (optional)
% #2 range of options to turn on
% #3 max number of options to create
\newcommand{\@AMCrowchoices}[3][]{%
  \pgfkeys{#1}%
  \def\@@tmp{}%
  \foreach \i in {0,...,\numexpr#3-1\relax}{%
    \global\@addedfalse%
    \foreach \j in #2{%
      \ifnum\i=\j%
        % using \n from onerow macro
        \xappto\@@tmp{\noexpand\correctchoice{\i}\if@multiques\noexpand\scoring{b=\@AMCrowChoiceScore,m=0}\fi}%
        \global\@addedtrue%
      \fi%
    }%
    \if@added\else%
       \xappto\@@tmp{\noexpand\wrongchoice{\i}\if@multiques\noexpand\scoring{m=0,b=0}\fi}%
    \fi%
  }%
\@@tmp%
}

\newenvironment{tablequestion}[1][]{%
  \pgfkeys{table width, num columns, column width, column height, score, left text width, table header left text, use multi answer=false, use options number, #1, set text width}
  \let\oldAMCbeginAnswer\AMCbeginAnswer%
  \def\AMCbeginAnswer{}%
  \let\oldAMCendAnswer\AMCendAnswer%
  \def\AMCendAnswer{}%
  \let\oldAMCanswer\AMCanswer%
  \def\AMCanswer##1##2{\AMCtableCol{##1}}%
  \let\oldAMCbeginQuestion\AMCbeginQuestion%

  \@AMColdCount=\theAMCquestionaff\relax%
  \def\AMCbeginQuestion##1##2{\par\noindent}%
  \global\@AMCtableI=0%
}{%
  \let\AMCbeginAnswer\oldAMCbeginAnswer%
  \let\AMCendAnswer\oldAMCendAnswer%
  \let\AMCanswer\oldAMCanswer%
  \let\AMCbeginQuestion\oldAMCbeginQuestion%
  \setcounter{AMCquestionaff}{\@AMColdCount}%
}

\newcommand{\AMCtableLeft}[1]{\begin{minipage}{\@AMCtableTextL}#1\end{minipage}}
\newcommand{\AMCtableCol}[1]{\begin{minipage}{\@AMCtableC}\centering\scriptsize #1\end{minipage}}

\newcommand{\multiitem}[4][]{%
  \pgfkeys{#1}%
  \global\advance\@AMCtableI by 1%
  \ifodd\@AMCtableI%
    \global\def\AMCtableColor{gray!25}%
  \else%
    \global\def\AMCtableColor{white}%
  \fi%
  \par\noindent\begin{minipage}{\@AMCtableL}\colorbox{\AMCtableColor}{%
    \foreach \i [count=\n] in #4{}%
    \ifnum\n>1 \booltrue{@multioptions}\else\boolfalse{@multioptions}\fi%
    \ifboolexpr{bool{@AMCusemultianswer} or ( bool{@AMCuseoptionsnumbers} and bool{@multioptions} )}{\booltrue{@multiques}}{\boolfalse{@multiques}}%
    \def\content{%
      \AMCtableLeft{#3}%
      \begin{choicescustom}[o]
        \scoring{%
          v=0,% v: non answers (blank), e: incoherent (many checked)
          \if@multiques b=0\else b=\@AMCrowChoiceScore, e=0, m=0\fi%
        }%
        \@AMCrowchoices[#1]{#4}{\@AMCtableColNum}%
      \end{choicescustom}%
    }%
    \ifbool{@multiques}{%
       \begin{questionmult}{#2}\content\end{questionmult}%
    }{%
      \begin{question}{#2}\content\end{question}%
    }%
  }%
  \end{minipage}
}

% macro to automatically create open choices
\def\@oc@extra{}
\pgfkeys{
  open choice/.cd,
  lines/.store in=\@oc@lines,
  lines/.default=6,
  max/.store in=\@oc@max,
  max/.default=4,
  step/.store in=\@oc@step,
  step/.default=0.5,
  % unknown keys are assumed to be options to be passed to the amc package
  .unknown/.code={\edef\@oc@extra{\@oc@extra\pgfkeyscurrentname=#1,}},
}
\newcommand{\AMCOpenChoices}[1][]{
  \pgfkeys{%
    /pgf/number format/fixed,
    /pgf/number format/precision=1,
    open choice/.cd,
    lines, max, step, #1}
  \edef\@oc@extra{dots=false, lines=\@oc@lines, \@oc@extra}
  \expandafter\AMCOpen\expandafter{\@oc@extra}{%
  \pgfmathparse{\@oc@max-\@oc@step}%
  \foreach \i in {0, \@oc@step, ...,\pgfmathresult}{%
    \pgfmathprintnumberto{\i}{\tmp}%
    \wrongchoice[\tmp]{\tmp}\scoring{\i}%
  }%
  \pgfmathprintnumberto{\@oc@max}{\tmp}%
  \correctchoice[\tmp]{\tmp}\scoring{\@oc@max}%
  }%
}

% patch space in center
\renewenvironment{center}
 {\parskip=0pt\par\nopagebreak\centering}
 {\par\noindent\ignorespacesafterend}

% patch explain macro to split boxed questions
\renewcommand{\explain}[1]{%
  \ifAMC@correchead%
    \ifAMC@qbloc%
      \AMCif@env{question}{%
        % cloe previous minipage
        \end{minipage}\vspace{\AMCinterBquest}%
        % normal definition of explain
        \par\noindent{\AMC@loc@explain #1}%
      }{\AMC@error@explain}%
      \vspace{1ex}%
      % then we need to close the default minipage from the question
      \noindent\begin{minipage}{\linewidth}%
    \else%
     \AMCif@env{question}{\par\noindent{\AMC@loc@explain #1}}{\AMC@error@explain}\vspace{1ex}%
    \fi%
  \else%
    \AMCif@env{question}{}{\AMC@error@explain}%
  \fi%
}