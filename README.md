# Multiple Choice (`adnamc.cls`)

The class `adnamc.cls` let us create multiple choice exams, and similar documents (like surveys) using randomly shuffled and generated answers and questions. It extends the functionality of the [`auto multiple choice`](http://home.gna.org/auto-qcm/) package. 

## Pre-Requisites

- Install [`auto multiple choice`](http://home.gna.org/auto-qcm/) package (at least version 1.3.0, for latest versions check the [installation webpage](http://home.gna.org/auto-qcm/download.en))

    ```bash
    sudo apt-get install auto-multiple-choice
    mkdir -p ~/texmf/tex/latex/AMC
    ln -s `locate automultiplechoice.sty` ~/texmf/tex/latex/AMC/automultiplechoice.sty
    ```

    Normally the package installs the style at `/usr/share/texmf/tex/latex/AMC/automultiplechoice.sty`. In case the `locate` command fails, you need to update the database definitions with `updatedb`.

- The example depends on other packages:
  - [`adnlogos`](https://gitlab.com/adn-latex/adnlogos)
  - [`codetools`](https://gitlab.com/adn-latex/codetools)

## `adn-latex` Group

This package is part of a larger group of repositories that work together. Mainly, I develop them to perform different tasks on LaTeX.

For an more information on the group and how to obtain them visit the [`adn-latex`](https://gitlab.com/adn-latex/adn-latex) repository.